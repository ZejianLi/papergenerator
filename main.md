
# Paper攻略

## 总体流程

总体来说分成以下几个阶段
* 构思阶段（总体架构）
* 雏形阶段（第一版初稿，基本内容确定）
* 连通阶段（第二版初稿，**逻辑通顺**，配图完备，引用到位，基本具有paper的样子）
* 修饰阶段（第三版初稿，表达清楚，格式美观且符合要求，基本无语法错误）
* 援审阶段（交换审阅，导师审阅，按意见修改）
* 终稿阶段（形成最后版本，阅读全文并最后细调，更衣沐浴焚香祈祷）

构思阶段之后应该开始准备配图，雏形、连通和修饰阶段必须在两周之内完成。

## 构思阶段

构思阶段的目的是确定paper的总体结构，大概写什么内容，并且在纸上画下草稿图（concept sheet）。这一阶段主要是放松思想，把所有想到的都加进去，作为之后写作的一个路线图。我们之所以面对paper无法下笔是因为我们不知道要写什么，这一部分就是要初步解决写什么的问题。

第一步，回答以下几个问题并写下答案。
1. 这个研究的目的是什么？
2. 谁会来读这个paper？
3. 这些读者读完这个paper应该知道什么？

第二步，找来一张纸，完成草稿图。画出任何你想写的内容，不拘于任何格式和逻辑，想到什么画什么。有几个问题可以引导。画的时候可以用箭头指示个部分的连接关系。
- 为什么你要做这个研究?
- 它的背景是什么？
- 你如何思考这个问题？
- 你做了什么工作？
- 你觉得未来的发展方向会是怎样？
- 还有其他什么相关的内容应该在这个paper里面
- 用什么图来表示？这个图应该是什么样子的？

画好了之后，上述内容应该拍照整理。

（发表paper就类似市场营销，你需要明白这个市场上有什么消费者，他们需要什么样的产品。[2][^2]）


在这个阶段，还需要考虑这个paper应该发表到什么平台上，哪个期刊或者哪个会议，并且开始阅读目标刊物或者会议相关方向的论文，以及其他类似平台上面的论文，着重关注最新的论文，并且尝试在谷歌学术上面搜索引用他们的新论文。这样做的目的有几个：
第一，了解目前本领域最前沿的工作的侧重点，比较看看自身的研究目的是否和前沿工作的研究思路相悖。
第二，了解前沿做到了什么地步，明白本研究相比起来基本上是comparable还是outperform。
第三，观察现有工作的表达和论述逻辑，以及哪些内容需要之后深入介绍而哪些并不重要。
第四，明白自己应该引用哪些论文，尤其是目标期刊论文，不管目的是比较现有工作还是“取悦”潜在审稿人。
最后，选择时间和水平都合适的发表平台，确定首选和备选方案。



## 雏形阶段

雏形阶段的目的是根据草稿图开始完成第一份初稿。这份草稿应该已经把paper的主要部分完成好，尤其是理论基础部分，所提出方法部分和实验部分。开始雏形阶段之前应该按照构思阶段的架构，把实验数据和所需要的配图准备完毕。

这个阶段开始的时候先回答一下问题并写下答案：
1. 你用什么方法实现了第一阶段所回答的研究目的
2. 你用什么证据说明你实现了（理论上的保证或者实验的结果）
3. 你用的方法的逻辑出发点是什么（某些假设Assumption，某些观察的现象）
4. 不考虑严谨性的话，你的方法有什么直觉上的理解

根据上述两次问题的答案，开始列提纲和整理实验计划。这个提纲在上述草稿图的基础上整理完成，应该做到清晰明了尽可能详细，开始构造文章的逻辑链条。此时一般实验也做得差不多，列实验计划主要是看看还有什么实验应该补充和完善。列提纲和整理实验计划最重要的功用是为了梳理整个文章的逻辑脉络。

有了提纲，接下来开始一点点写草稿。一般来说先写Method部分，这个是主体。之后是实验部分，Related work，Introduction，Conclusion最后Abstract。可以随意调。下面说明每个部分应该写什么。

### Abstract
Abstract是对整篇paper较高层面的阐述，应该积极简单，一般100 words左右，最长不超过200 words。Abstract可以按一下小点组成，每点不超过三句话。
1. 这篇paper讨论什么问题。
2. 该研究问题很重要。
3. 现有工作存在一定不足。
4. 概括本文的工作。
5. 描述本文工作的新颖性在哪里。（这点不是必须）
6. “Extensive experiments show ...”本文提出方法有优越性。
7. 可以把突出的指标直接接到最后，强调performance。

（读者拿到一篇paper都先看abstract，再看之后的配图和conclusion。所以Abstract必须写好。　[1][^1]）

### Introduction

Introduction要概括paper的主要内容，是对abstract的每一点的进一步叙述。有一些评审甚至读完Introduction就决定是否接收了，连正文都不看。主要有几个部分组成。
1. 本文研究什么问题。（This paper is concerned with the problem that... / This paper is to investigate）
2. 为什么这个问题很重要。
3. 前人大概做了什么工作，有什么探索。
4. 你在这个研究里完成了什么工作，完成了什么，怎么做的，大概思路是什么。你做的什么内容是其他人没做过的，有什么创新点。
5. 实验结果如何，验证了什么
6. 本文的主要贡献是什么？（In summary, the contribution of this paper are three-folds... ）


第一小点相当于abstract里面对应点的进一步解释性描述，说明本研究的motivation是什么，要达到什么目的，怎么定义问题，甚至框定scope。
尤其对于会议论文，假设review文章的都是大同行或者小同行，那么大方向的重要性和成功就不必复述了，Introduction最好上来直切文章关注的核心点，而不要浪费太多笔墨在冗长的背景和现状介绍上，避免引起反感。而如果是期刊论文，采用history的叙述方式连贯地介绍背景还是可以的。

第二小点是研究问题的背景和意义。要说明为什么这个问题重要，可以指出过去曾有研究者阐述过这个问题的和重要性，并且引用多篇关于此研究问题的应用类文章，或者说明这个问题能够对现实世界中的设计问题、工程问题和一些应用产生影响。[1][^1]

第三小点是小综述，不宜太详尽。此处描述前人的工作是高层面的描述，切忌列举，应该按照自己想的思路梳理出一个分类出来，然后按分类总结，或者按时间线或其他逻辑线描述这个literature的发展流程。

第四点相当于方法部分的总述，先把现有工作的不足复述一次，之后说明你的方法的立足点（例如某个假设或insight），之后是方法本身。这类似于把Method部分的每一段用一句话概括最后串起来。这部分切忌加入细节，也最好不要有符号公式。立足点和观察insight有时候比方法重要，需要重点说明。最后讨论创新和新颖性在哪里。

第五点是实验部分的概括，一般是“Empirically the proposed model  outperforms the state-of-the-art methods in terms of ....”。除了概括描述，可以把突出的指标写上来，强调performance 的进步。并且把ablation study的结论也写上。

第六点是总结main contribution。分点说明你的paper有什么贡献，例如新的理论分析，新的方法，新的实验观察，等等。如果某个点是你首次提出的，那么可以说 To the best of our knowledge, this is the first time to ....
现在有些paper会吧Abstract的第六点作为一个contribution写下来凑够三个contribution，相当于混淆了highliht和contribution，不是迫不得已不要这样。
一般地，contribution应该和研究问题对应上，也就是针对本文要解决的问题，而实验部分里则有验证contribution有效性的实验，support contribution的claims。有一种写法是Conceptual contribution，technical contribution，experimental contribution，分别加粗作为各个contribution的总结，后面详细描述，装作比较清晰。

每个段落可以用一个词组加粗作为开头，概括本段的核心/元信息，连起来也是文章思路的概括。例如Background/Problem definition/Hypothesis/Method/Experiments等。

老的论文会在最后说本文的结构（The rest of the paper is organized as follows.）现在大家都不这样写了，因为既没有传达什么信息也对文章把握不会有什么帮助，尤其是点出conclusion什么的。
有一种比较好的写法是整个文章的sections之间是有逻辑关系的，在introduction里面复现这样的逻辑关系，并且在对应位置标注section 和 figure的引用，这样就顺便把文章organization也点出了。即便读者看完忘记，也会有好印象。如果整个逻辑关系不好echo，section乱序标注也是可以的。

找contribution的时候可以多想想其他方面，例如是不是orthogonal to current contribution，或者有其他新特性。



### Method

这个部分详细描述paper主要内容。可能包含以下几部分，有些可能会省略：
- Preliminary
- Assumption
- Theorectical foundation
- Method
- Complexity

可以有Overview + 细节的写法，或者Overview + Part A + Part B + Part C 等等，或者按照演绎过程。按照contribution的突出需要选择结构。

一般做的paper都是基于前人工作，因此需要一个Preliminary介绍本文所基于的方法，或者在这部分对所用到的基本数学框架和符号进行定义。可以做一个表格总结全文用到的符号及其含义。

Assumption，方法提出的出发点。可以是某个Observation，比如某个常识，某个观察到的现象，又可以是某个其他领域的结论等等。Paper本身是一个演绎推理过程，而所基于的出发点都应该在此处清晰定义，所有的term/专用术语/最基本的概念都需要明确定义。可以是文字描述，也可以用数学环境写清楚。这个几乎所有 paper 都显示或者隐式应该具备。过于反直觉或非公认的Assumption应该给出某些支持的理论或例子，或者预实验验证。

Theorectical foundation和Method部分是根据具体的文章内容来确定。基于paper要简单容易理解的原则，我们有以下几条意见。
- 每个段落有一个中心句，整个段落围绕整个中心句展开。
- 机器学习算法组成部分有三个，模型、目标函数、优化方法。所以可以分这三块分别描述。
- 如果是深度学习算法，模型相当于网络结构，可以放在实验或者附录描述。
- 用一张图描述你的方法，输入是什么，输出什么，中间每一个地方怎么处理，这个最好要有
- 先语言简单描述要说内容的直觉理解（The intuition of our method is that），再给出形式化的定义（Formally, ...），最后给出理论上的依据
- 复杂的概念可以举例子或者给出某个类比
- 例子可以是在toy dataset上的实验结果，可视化展示
- 要证明的定理推论等，命题在正文，证明放在附录。
- 基于方法和实验分离的原则，这里应该只说明方法的基本思路，具体实现细节（例如如何调参用什么库编写等等）放Experiment或者method 最后加一个implementation details
- 最后可以给出符号版的算法流程，篇幅不充裕可以不给

相对传统的文章需要给complexity分析，就是时间复杂度空间复杂度分析等。如果基于深度学习的就没什么好说了。但是可以有参数量的分析和computation的分析。

（有些paper是方法简单，但是有理论支撑，进过复杂的数学推导证明其性能。这时候他们先把方法流程说清楚，之后再说理论基础，甚至把理论基础放在实验部分之后。还有scientific report上面会把实验放在第二部分，之后再说method。所以文章结构还是比较灵活的。但是一般范式比较保险。）

方法部分的写作有时候比较依赖公式的表示和符号传达，在涉及推导的文章更是如此。但是一般人第一次扫文章的时候肯定都忽略公式，因此应该尽可能保证，如果所有符号都没有了，所有公式都删掉，剩下的描述性文字是否足够表达整体的含义。

写作的时候应该注意取舍，取舍的标准以前面规定的本文目的和本文应该传达什么为参考。Deep Learning的论文可能涉及很多的实现细节，在会议论文甚至期刊论文里面都不可能把所有的细节交代清楚，目前也很难根据一篇论文进行代码复现。所以一定要区分什么是重要的，什么不重要，重点放在真正有contribution的部分，没有contribution的部分弱化，不要让审稿分心到其他没有contribution的地方。

现在很多论文可能有预实验部分。相当于前面的Assumption部分或者Prelimiary。预实验目的在于motivate 后面的方法，明确本文解决的问题确实是问题，验证方法所基于的观察、insight、hypothesis或者其他出发点。并且顺便提前展示实验结果，给读者实验结果很好的先入为主印象。更加避免了上来就是方法和公式。而方法和预实验前后对照也可以显得逻辑更连贯。现在非常推荐*有条件的时候增加预实验*。

Method 部分只要需要两张图，一张是整体算法流程，一张是主要contribution的细节。尤其是要解决的核心问题需要在图上标注或者有效传达，并且体现问题确确实实得到解决。流程的展示为表，contribution凸显为里，配图要和文字部分呼应，符号也要对应。

目前method部分还应有方法的讨论，例如为什么这个方法能够解决问题、克服了问题分析里面哪些关键点、为什么不能用更简单的方法解决、从各个层面对比与其他方法的差异、各种每个设计的环节的作用和用意是什么、收敛和训练的难点是什么、到底哪些参数是优化出来的哪些是迁移的。

Training的部分一般是loss、model、优化器、训练策略等，可以分开，小标题加粗显示。


### Related Work

Related Work部分描述跟你做类似问题的相关工作。介绍的时候同样不要罗列，要按某个逻辑顺序描述。比如基于同样结论或者同样模型的等等。每一类方法介绍完说明你的方法和这些方法有什么不同，优越性在何处。有些理论或形式上的对比可能放在Method部分更好。

Related Work的目的是为了小综述现有研究，顺便衬托你工作的贡献在哪里。一个常见的误区是Related Work里面写了很多自己知道的总结出来的相关工作，但是其实和文章研究主题相关性比较低，显得跑题。所以，每个Related Work介绍的时候，首先问问自己，为什么要介绍它，介绍的目的是什么。会议论文中引用的篇幅比较有限，每个引用都需要珍惜使用。

准备论文写作的时候可能重新读相关论文，可以顺便进行related work的材料准备，梳理清楚工作的不足。

现在流行related work的每一段都是加粗开头的表示本段综述什么。

（以往Related Work放在Introduction之后。但是越来越多人把Related work放在Experiment之前。原因有两个。第一，尽快让读者了解你的工作，而不是现有的。第二，介绍了自己的方法之后，再说别人方法时就可以顺理成章说差异的地方在哪里。如果在Method之前写了Related Work，逻辑就乱了。另外甚至有文章把related work写在experiments之后conclusion之前，都是可行的做法。）



### Experiment

实验部分展示所有的实验结果，并比较和分析，为introduction所说的contribution作实证支撑。
这里包括几个部分：
- 实验的目的有哪几个，基本上要对应introduction说的研究目的和contributions
- 所用的数据集是什么（有时候可以按数据集分小节说明结果，所以每节介绍一次数据之后是结果，那样也很清晰，需要做的实验也少）
- 实现细节，所用的模型（网络结构）、优化方法、实现环境、超参数、有哪些trick，有什么地方为了方便和formal定义描述的不同等等，加上必要的原因说明。
- 每个小实验的目的是什么，如何设计这个实验的，出来的结果是怎样的，用了什么指标来量化比较，或者用了什么大概的比较方法，比较之后得到什么结论
- 近年来很多paper会进行消融实验（ablation study），也就是算法当中每个小部分去掉或者组合式地去掉看看结果如何，从而验证每个小部分的必要性。这个可以模仿。
- ablation study 目前是标配，考察模型中各个部分的重要性。可以在performance的后面加上和原始模型的数值变化，强调ablation的差异，就是(+- xxx) 表示差距。Ablation study可以逐个检查主要contributon的作用，或者使用的其他tricks的作用。有一些审稿人会希望像stylegan2一样逐个trick加上去，得到最终效果，看着指标不断优化。

本阶段可能实验数据和配图没有准备完毕，可以先准备好表格的框框，还有准备每个figure应该展示什么，先空出来之后去准备。
比较最好加上显著性检验，做到p值小于0.05。但是注意选择检验的方法
比较之后有什么结论结论可以写上之后注释掉，完成之后没问题了再取消注释。

实验的目的比方法重要，实验可以不只是为了验证有效性比个高下，如果可以专门额外设计实验来明确未来发展方向和潜在问题也是好的。
生成实验多考虑算法的最显然要探讨的特性，例如生成的diversity，few-shot里面K的影响等等，也就是主要超参的影响 。
良好的实验设计在目前的CV类paper中占比比较大，在方法不突出的时候尤其重要。

新的写法是Experimental Protocol和Experimental Results分开写。把目的、数据集、base lines、metrics等写在前面。主体实验结果、ablation study、component的特性对照 和 其他讨论实验结果，每个实验按照 实验目的-结果描述-实验结论 来编写。实验的表格可以加粗表头，指标旁边标注上下箭头表示越大/小越好，最好的指标值粗体，指标前面加上\mathcal{y}等等表示哪里copy的。所有指标最好是多次实验的平均，后面+- std，可以缩成上标。
如果可以尝试使用真实数据等方法估计一个指标的上/下限，那么能够让读者知道未来提升空间多大，是一个大牛团队的作风。

方法当中一些要学习的关键参数，可以给出最后的值或者画出曲线图，让读者知道关键参数最后学习成什么样的。自己提出的指标，尤其无量纲的指标，可以尝试给出一些可理解的中间参考值，让读者对值的含义有一定了解。例如熵是无量纲的，但是0.1:0.9 和 0.2:0.8 的伯努利分布的熵值可以作为参考。Qualitative result的图最好都是cherrily picked的，不要用随便挑的图硬钢。

### Conclusion

结论部分应该再次说明本文的研究问题，之后复述方法的主要流程，并且描述实验结果，诚实地说明方法还有什么缺陷。
结论需要强调这个论文论述和实验之后有什么结论性的东西，或者读者应该知道的应该记住的内容。
一般来说future work都没有价值，所以要不不写要不写一下相关领域未来发展的方向和原因，总之就是写读者看来会觉得有价值的。
结论部分不要直接复制摘要，应该尽量体现出paper的价值。

（Journal paper可能还会有discussion部分，对方法和原理进一步升华。Conference里面首先篇幅应该不用写。我也不知道Discussion应该写什么。）


至此，雏形阶段完成。可以检查一下，abstract里面的内容在introduction里面是否都有展开都介绍清楚了，相应地正文中是否有详细严谨的论述。Introduction 的contribution应该在正文里面都介绍清楚，作为逻辑链条中重要的一环讲清楚前因后果，怎么想到怎么保证怎么推导，会产生什么影响，在后面的实验中怎么验证，在数据上、指标上或者其他比较上如何体现。如果可以，把paper写到目前阶段的内容抽取出来形成一个presentation，将一次给其他人看看，开始寻求第一次反馈。



## 连通阶段

本阶段需要整理第一版的初稿的前后逻辑衔接，把配图、表格，及其解释性文字都表述好。完成Reference，Acknowledge部分和Appendices部分。


### 逻辑衔接

逻辑衔接分宏观部分和微观部分。宏观部分设计逻辑论述的完整性。好的逻辑有几个充分条件。
1. 逻辑结构完备
2. 论据和论点相关
3. 论据是可接受的（acceptable）
4. 所有论据加起来足够说明论点
5. 对一些可能的反驳、质疑和疑问提前作解答

在我们的paper中，一般围绕一个论点展开：本文的方法实现了研究的目的，并且具有创新性和优越性。
第一阶段成稿说明了研究目的，还有所提出的方法是基于什么假设，如果推出方法的过程也比较严谨的话，点出了和已有方法的不同，加上有理论证明或者实验例证方法的优越性，那么第一个条件基本达成了。论据一般是理论上证明或者实验中比较performance，一般相关性和可接受性不会又问题。第四点，创新性有主观因素，而对优越性，实验中显著性检验通过了，那么就有足够说服力了。
第五点是最容易忽略的，也是遇到了paper写不长可以用来补救的地方。从读者或审稿人的角度想一想可能有什么质疑或者疑问，然后用One may wonder/ One might argue that ...自问自答一下，那么paper的逻辑就会更完整。

宏观的逻辑需要确保每一个结论都有足够的前提假设、讨论和实验数据支持，前后呼应，确保无懈可击，用以博取审稿人肯定并抵御潜在的质疑。可以考虑每个推论的逻辑是什么来的？有充足数据支持么？有没有某个设计参数没有讨论？还缺了哪个实验结果？如果缺失就要补充重写。

微观部分，一般是指句与句之间的逻辑链接，段落之间的承上启下，前后文的呼应等等。英语写作讲究逻辑，连通阶段就是要把大大小小的逻辑关系疏通。
句子之间的连接一般用连接词来体现逻辑关系。常见的先后关系（first/second/next/finally...），因果关系（Because/Accordingly/As a result of this....），转折关系（But/even though/nevertheless），递进关系（furthermore/more specifically/additionally），比较关系（in comparison with／when compared with），对照关系（On the contrary/Unlike/conversely）还有举例、条件、强调、总结等等。表示时间和地点关系的在scientific writing中比较少见。
段落承上启下是指上一段结尾和下一段开头有衔接。一般把下一段开头的第一句取个中心词过来在上一段结尾引出就可以了。但是这种伎俩不能做太多，否则很无聊。
前后文呼应是指有时候后文用到了前文定义的概念或者某个点，那么用recall that... 复述一次，相当于把第一阶段的concept sheet里面的连接箭头整合进paper。

另外在本阶段，必须保证所有用的符号都必须被定义好，文字描述或者用数学环境定义都可以。并且保证符号和某个含义是一一对应的。一个符号全文只表示一个意思，一个意思全文仅用同一个符号表示。并且，所有用的到的缩写也在第一次出现的时候定义好，除非是业界公认的缩写（例如PCA，SVM等）。


### 图表

读者可能连Abstract都不看，就会翻看图表，因此图表非常重要的。图表有几个原则可以遵循。
- 每个图表做之前应该先想好这个图表展示的目的是什么，画出来之后衡量一下是否达到目的
- 每一幅图都应该相对独立完整，而不过于依赖其他部分，满足读者可能上来就看图的习惯。
- 每一张图表都应该有一个标题，之后配上解释性的文字。标题可加粗，但是只有首单词首字母大写。
- 基于图表应该和引用及描述的文字尽可能靠近的原则，一般使用latex的强制命令!固定图表的位置。如果和引用实在不能在同一页，图表的描述放在caption里面是很好的方法，但是图表的评论和分析则不要在caption。所有的图表都要在正文被引用。
- 每一个坐标图的坐标都应该标注出来是什么意思。每一条曲线都应该标注好是什么曲线。
- 图表应该有侧重的地方，一般表格把performance列出来，最好的变粗体。其他Figure可以加个框框告诉读者注意什么地方。
- 保持图片里面的文字和正文文字字体一致。图片的文字放缩之后可能比较小，一开始应该把字体做大一些。尤其是希腊字母，希腊字母比英语字母小一个档次。一般会议和期刊对图表字体大小也可能有要求，例如IJCAI是要求 9 point。考虑到放缩不可能完全符合，但IJCAI的正文字体是10 point，只要不是相差太明显就好。
- 图表或者表格里面，如果数字的零太多，也可以用单词替代以保证美观。但最好所有数字都使用相同的形式。
- IJCAI 对line weight 要求在0.5 point 以上，所以作图时候最好设大几倍，免得缩小了图之后就不符合规格。
- 有些会议和期刊要求黑白图像，但能用彩色尽量用彩色。
- 表格里去掉竖线，减少主体部分去掉横线（不用 \hline）会更美观，也显得不那么像新手。表头的字体可以加粗，也会更美观。
- 表格可以采用[l\*{4}{c}r]类似的对齐命令，这里是6列的。最左边的左对齐，最右边的右对齐，中间的居中，会相对美观。
- 近年来CVPR和其他顶会上面大规模出现了“首页图”策略，也就是在文章的第一页就用图表展示本文工作的核心内容或研究目的。这个特征在何恺明的论文上表现很明显，也有很多纷纷模仿。因此，对于会议论文，排版合适可以采用类似策略。在首页图上可以展示算法的基本流程、经验的效果或者问题的定义。按照贡献不同，确定首页图的目的和传达信息是什么。
- 另外，大量顶会论文也出现了模型架构图的展示，也就是用架构图展示模型的结构、如何训练、数据如何流入和流出、甚至梯度怎么传递的等等。这些图表都非常精美，坊间流传大团队做paper时候以流水线分工合作形式，专门有人负责专业作图。因此，为了增加paper竞争力，精美的架构图也是必须的。
- 所有图表风格最好保持一致，所有图表放在页面的四个角，尽量避免上下都是文字。所有图表的颜色不要太鲜艳，也不要使用老版Office的默认配色。
- 表格使用toprule midrule bottomrule等三线表效果也会很好，使用\footnotesize缩小字体，免得太大占空间.
- 引用图表的时候可以用Fig. Tab. 等缩写，注意有点，同样Section可以变成Sec.。
- SM中的table 和 figure可以使用S1 S2来区分标记。


### Acknowledge部分

致谢部分一般谢三部分。谢人，谢funding，还有谢谢数据提供者。谢谢的人都必须有全名，以及其所属的单位，还有谢谢他的原因。谢funding要说明机构。（
This work is funded by the National Natural Science Foundation of China (NSFC) under Grant NO. 61773336.） 谢谢数据提供者是必须的。很多数据的官网有使用数据的致谢和引用要求，都应该遵循。


### Reference部分

每一个应用都尽可能完整，应该包括作者的姓名，整个标题，所出版的年份，发行的期刊或者所在的论文集（proceeding），第几卷开始以及结尾的页码等等。如果是书的话，还要说明出版商,出版商所在的国家，第几章，开始和结束的页码。如果是在线的资源，必须说明清楚你最后一次访问是在什么时候。很多已正式发表的paper，有人还会引用其arxiv版本。未知这个做法是否合适，但没有被吐槽就可以这个干。
每个会议和期刊都有guideline规定所要求的格式，必须看清楚好好遵守。最好先做成bib格式的，方便调整。


### 附录部分

附录的每一个小标题后面都应该是有大写A开头，如A1、A2。附录一般写那些冗长乏味但可能不能缺少的细节，例如定理的证明。附录应遵循Method和Experiment部分的原则。有些人会在附录把更多的实验结果展示出来。


### 预演法
会议或者期刊可能有template提供，到了这个阶段的最后就可以填入以上文字内容，然后生成看看。
至此，连通阶段完成。文章基本初具规模，然后思考一个问题：
三个月之后收到的审稿意见全部是负面的，这个paper在第一轮被拒绝了。那么，这些负面的审稿意见会是什么？这个paper会是因为什么原因被否定？要用reviewer般挑剔的眼光去对自己的工作提出质疑，把所有想到的原因写下来，并且每个原因估计一个出现概率和一个致命系数，(0,1]，对应相乘，之后从高到低排列所有的可能性，并对应修改。
如果想到的原因是novelty or contribution is limited，那么做好最坏的打算。




## 修饰阶段

本阶段注重paper的writing style，纠正大部分格式和细节问题，以及语法错误。从头到位阅读一次草稿，按照语言的通顺性和表达的清晰性修改，之后再各条原则对照有没有问题。

[2][^2]里面一直强调的是清晰（clarity），简明（simplicity），结构完整（good structure）和用最容易理解的方式传递信息（convery information in the most accessible way）。因此有以下几个方面的原则可以遵守。

### 词汇
* 用短的简单词语而不是长的或生僻词语，但同时避免使用informal的口语的词汇。
* 选定美式或者英式英语，切忌两者切换。Sublime Text的词典里面可以选择词库。投欧洲的期刊一般选择英式英语，但不是必须的。
* 注意冠词a/an/the 的使用。除非抽象名词、专有名词、复数或者在标题当中，否则不应该省略冠词。在 paper里面，一般不是第一次提及的都要加the。
* 去掉口语类缩写而用正规表达does not/is not/cannot/will not....
* 去掉无意义的副词(very, rather, somewhat, quite, 或者 obviously，definitely，easily，really等 )，去掉评论性的形容词(the important result/this significant finding换成the result/this finding。让读者来判断是否important。)
* 少用过于绝对的词语，all, always, no matter 等等，除非有严格理论保证。
* 不要过量使用缩写，每个缩写也尽可能短。
* 两个形容词之间依然可以使用逗号，虽然比较少见。（It is a cold, rainy day.）
* 不要在正规写作里面使用etc., i.e. 和 e.g. 等等缩写词，必须用全拼形式。如果不怕死真的要用，当etc., i.e. 和 e.g. 出现在句中时，前后使用逗号。（We ate chips, nachos, pretzels, etc., at the game.）etc. 在句末就不需要额外句号。
* 在科学和技术写作里面，表示一到十的数量要拼写出来 one/two/.../ten，十以上的用数字。然而人文学科（应该包括设计类）一到九十九的数量都要拼写出来，而二十一到九十九要用连字符连接两个单词（ninety-nine）。数词如果出现在句首则应该拼写出来。如果一个句子里面有两个相关的数字，那么必须用同一种形式，要么都拼写要么都是数字。有些审稿人甚至希望看到一个段落里的数字形式一直，要么都拼写要么都数字。
* 出现大于等于1000的数字需要用逗号每三位隔开。同时出现两个数字的时候也要逗号隔开。(In 2015, 350 students graduated from this school.)
* 先行词是被代词所指代的名词。使用代词的时候要确保代词指代明确，即先行词明确。This, these 和 which 常会出现指代的先行词不明的情况，使用的时候要问自己这个this, these是指什么，会不会有歧义。
* 选择强势精准的动词会使写作更加生动简洁，也不需要那么多副词。这个需要词汇的积累和熟练使用。但可以确定的是 There be 结构是弱势结构，应该避免使用。
* 词语或者某些定义是否应该首字母大写或者加小横线，可以参考wikipedia或者其他已经发表的paper。
* 英文句子的开头用大写字母来表明时，如果是一个数字，就要把数字用英文写出，或修改句子结构不用数字开头。一个经常使用的方法就是把表示计量的数字放到括号里面去。
10 mL ethanol was added. 改成 Ethanol (10mL) was added.
* 当两个数字前后并列出现时，一个要用英文字，一个写成数字，例如：
3 8-rat groups 要改为 three 8-rat groups，
two five-day study 要改为 two 5-day study
* 小于1的数字的单位用单数，大于1的数字的单位用复数，例如：
0.25 gram, 0.8 second, 1.5 grams, 3.45 seconds。
但是零后面的单位用复数 0 meters。
* 把oral的词汇改为formal的词汇，从而更像是书面写作
|  Informal |                      Formal |
| --------: | --------------------------: |
|  a lot of |                  much, many |
| do (verb) | perform, carry out, conduct |
|       big |                       large |
|      like |                     such as |
|     think |                    consider |
|      talk |                     discuss |
|   look at |                     examine |
|       get |                      obtain |
|      keep |            retain, preserve |
|     climb |                      ascend |

### 句子
* 用简单明了的语言和结构，避免长难句和复杂结构（例如独立主格结构或分词短语结构）。
* 正规写作应使用完整句，而不可使用残断句。残断句即只有从句没有主句。（Because I was late.）这样句子只有从句没有表达完整的逻辑。改正的方法是把主句和从句重新组合为完整的句子。
* 同样的写作里不可以使用连排句。连排句就是两个完整句子仅用逗号分开。（We went to the movies, our friends went to the museum.）改正的方法可以是分成两个句子来写，或者按两句的意思用连词连接起来。
* 宾语从句前面最好都加上that。
* 除非是较长的介词短语或者两个介词短语并排使用，否则句首的介词短语之后无需逗号。（On Monday after lunch, we went to the movies.）
* 并列句用并列连词连接两个完整的句子。除非两部分特别短，否则在并列句中会在连词前面使用一个逗号。如果一个词语可以当连词，但两边不是完整的句子，那么此时就不是并列句，就不可以使用逗号。（She brought her brother home and made him dinner.）
* 不要使用双重否定来表示强调以避免复杂的句子结构。
* 不确定某个表达是否地道，直接google一下看看有没有人用，怎么用。
* 句子一般不以介词结尾，尤其绝对不可以用at作为最后一个单词。
* 正规写作中不以并列连词作为句首单词单词。并列连词包括for, and, nor, but, or, yet, so. 但是for作为介词（例如 For a training sample $x$ ）出现在句首也是可以的。
* 从句在句尾时，because和其它从属连词前面不加逗号。主从句再长也不加。（I missed the bus because I was late.）
* 每句话考虑一下会不会有歧义的情况出现，然后尽可能消歧义。
* 当你讲述某个paper或者某个著作的时候，一般应该使用现在时态而不是过去时，因为你所描述的对象依然存在。


### 标点符号
* 分号;的使用比较少见。分号前后句子要不结构相似，要不内容相近。若非这两种情况就不要用了。并列句不使用连词的时候，可以用分号代替，分号后面的句子不需要首字母大写。公式中用分号请看下面。
* 科学写作里面很少用感叹号!。最好不要用。即使要用，永远不要在一行里面出现两个感叹号。
* 短线 - 一般用来造复合词。自己造的最好也google一下，或者从已经下载过的论文里面搜索有没有出现过。
* 长线（dash）一般用来加插入语，科学写作同样少见。
* 双引号。一般来说，我们的paper引用某个人的原话的情况比较少，双引号一般只用在所用的词汇表示了特定语境下的含义的时候。例如使用了一些口语的表达，但是这个表达可以让读者对所表述的内容有形象的理解。但因为口语化的表达，在正规的写作当中是不应该出现的，所以加双引号。在latex中要用键盘波浪线~的那个对应的点点表示左引号，普通的单引号表示右引号。而双引号则是\`\` ''。切忌用普通的“”。如果是引用了别人的话，那么在英式英语里面句号放在引号的外面，而在美式英语里面则一般放在里面。如果问号和感叹号是引用的一部分，那么应该出现在引号里面，否则在外面。直接引语要用大写字母开始。(He said,"We want our pie.") 但是被分开的引用在第二部分就不需要首字母大写。
* 双括号和脚注一样可以给出补充说明的作用，不宜过量使用。双括号前后都留有空格。一般我们不会在正文用到中括号和大括号，尽在排版后引用以中括号出现，需要和前一个单词保留一个空格。
* 斜体用于大标题：书的标题、喜剧歌剧电视剧等标题。而引号则用于短的东西，章节标题或者短故事的标题。提及具体某个一个字母、某一个数字或某一个单词的时候用斜体。



### 段落
* 避免一个过长的没有切割的段落，以免读者阅读困难。
* 一个Section可以有铺垫，但不能太长，Subsection之间的关系要比较明确，不明确最好点出来。
* 觉得意思没有表达够的地方，尝试举个例子或者比喻个常识。
* 同一段落中时态尽可能保持一致。同一意群内的句子最好有相同的主语。
* 注意文章标题、各小节首字母大写。 一般实词首字母大写，虚词首字母小写。标题第一个单词、最后一个单词无论词性首字母应该大写。超过5个字母的虚词,如between、without、alongside、underneath等应该大写。标题总长度不超过20个单词，实词最好在10个以内。
* 标题大写标准: 第一个和最后一个单词必须大写；带连字符的单词两部分都要大写；冠词、连词、短于三个字母的介词首字母不必大写；单词is也是要首字母大写；短于三个字母的副词也要首字母大写。
* 每个段落的长度不宜过长或过短。过短太碎片，过长看着累。
* 所有出现的表格和图片都必须在正文中被referenced，否则copy editor会有疑问。


### 其它常见语法问题
* With regard to 而不是 with regards to。 Figuratively 表示比喻或者近似，而 Literally 就表示确有其事了。
* conduct experiment 而不是 do experiment。previous methods 而不是 old methods。our approach outperforms their methods 而不是beats their methods
* Toward 和 towards 意义相同。前者美式，后者英式。
* 使用比较级的时候注意比较比较对象错误。 She likes pizza more than me 表示相比吃我她更喜欢吃pizza。但是 She likes pizza more than I 就是她比我更喜欢吃pizza。最为标准的应该是 She likes pizza more than I do，但是现在大部分人都省略 do 了。
* Only是一个副词，不同的放置位置会表达不同的意思。We only made five dollars 是我们只赚了钱但是别的都没做。而 We made only five dollars 是我们赚的只有五美元但是可能还做了其他事情。
与此类似的还有almost。We almost made five dollars 是我们几乎赚了钱但是可能没赚到。而 We made almost five dollars 表示我们赚了几乎有五美元，可能是4.98刀。
* kind of 和 sort of 是赘词，大部分正规写作中应该避免。


### 公式
* 一般地，我们需要导入几个包。 \usepackage{amsmath} \usepackage{amssymb} \usepackage{bm} \usepackage{amsthm}
* 使用\boldsymbol{A} 表示各个符号，会加粗，更容易找到
* 实数域，自然数域等用空心字母表示。 $\mathbb{R}, \mathbb{N}$
* 定义集合和条件概率用 \mid 而不是 | . $P(a \mid b), \{x \mid x \ge 0\}$
* 表示某个概率用 \mathbb{P}
* 正态分布用的是 \mathcal{N} 而不只是 N
* 数学期望的E要用 \mathbb{E}
* 公式中的分号一般后面的内容表示参数，比如 \mathcal{N}(x; \mu, \sigma ) 表示这是关于随机变量x的正态分布，其中参数是\mu 和 \sigma。切忌与条件符号 | 混淆。有时其他统计量也会用到，例如x和y的互信息是I(x;y)。
* 自定义的统计量可以用 \mathcal{L} 来区分，比如变分下界
* 绝对值用的是 \left| 和 \right|
* 范数的两竖用的是 \\| \\| 
* 公式环境里面的省略号是\cdots 和 \ldots，中间一个圆点的是\cdot
* 方括号是 \left[ \right]
* 优化问题的变量写到arg min 下面 $x^\star = \mathop{\arg \min}\_x (x-1)^2$ 其中 \star 代表最优解。
* 函数映射 $f \colon A \mapsto B$。
* := 是 \coloneqq，需要 \usepackage{mathtools}
* 矩阵转置使用 \boldsymbol{A}^\mathsf{T} 或 \boldsymbol{A}^\intercal
* 矩阵向量要用粗体表示。 $bf{A}$，直立粗体。$\boldsymbol{A}$，斜体粗体。向量一般也可以加箭头标注，$\vec{a}$。一般矩阵用大写字母表示，向量用小写。
* 定义矩阵环境用 bmatrix.
* 在变量环境里面的字母都是以斜体出现，但斜体一般默认是变量，所以有几类符号和名字就应该用回直立体表示，用\mathrm命令。具体:
	* 有微元符号d，$\frac{\mathrm{d}f(x)}{\mathrm{d} x}$
	* 转置符号，$x^{\mathrm{T}}$
	* 量纲$24 \mathrm{m}^2$(量纲和数字之间还需要空一格，公式环境里面用\\,或者\\ 显示空格)
	* 优化问题里面的的s.t. (subject to)。 $\mathrm{s.t.}$
	* 函数名，例如$\sin(x), \exp(x), \log(x)$ 等。当需要自己定义函数名，要在引言去使用 \DeclareMathOperator{\sign}{sign}，之后就可以用$\sign(x)$.
* \arg \min 或者 \arg \max 要和后面的 s.t. 对齐，用aligned环境实现。例如
\begin{equation}
	\begin{aligned}
		\hat{U} = \arg\min \quad & \\| Y U - Y \\|\_2^2 + \lambda' \mathcal{R}(U), \\\\
		\mathrm{s.t.} \quad & \mathrm{diag}(U)=0.
	\end{aligned}
\end{equation}
* 公式也是句子，需要按位置加上标点符号。
* 定理证明用\begin{proof} \end{proof} 环境。


### 引用
* 所有元素作label的时候标注其类型，可以让自己更容易找。比如文献引用以cite: 开头，例如cite:lawry\_tang\_AI2009。还有section(sec:), equation(eq:), figures(fig:), tables(tab:), definitions(def:), theorems(thm:), assumptions(assmp:), etc. 
* 使用cleveref包可以自动为交叉引用加前缀，也可以自己定义前缀的形式。
\usepackage{cleveref}
\crefformat{equation}{(#1)}
\crefformat{figure}{Figure~#1}
\crefformat{section}{Section~#1}
\crefformat{table}{Table~#1}
* 还有可以让图片和公式按secition索引的，在长的期刊paper中比较有用
\numberwithin{equation}{section}
\numberwithin{figure}{section}
* CVPR 要求同时引用多个文献，cite引用的时候序号从小到大排列，自行调整。或许可以成为一个统一的习惯，多个论文引用都变成序号从小到大，或者年份从旧到新的。

上述问题无误之后就可以交给Grammarly查小的语法错误并改正，并且查找连续出现两个空格的地方改成一个。纠正之后基本不会被审稿人严重鄙视。

会议论文需要双盲审稿，正文中不应该出现任何能够推断出作者身份的线索，因此要把作者名字改成Anonymous，致谢部分也不能出现任何和具体某个人相关的内容。这些都可以先注释，paper中了之后添加。如果有什么代码或者数据集确定必然要公开作为contribution，但是审稿阶段不能泄漏，可以说明 "Our dataset will be made available by the time of publication."



## 援审阶段和终稿阶段

这两个阶段没有什么好说了。别人的意见应认真听取，刚愎自用是没有好处的。如果展示或者逻辑上有分歧，那么以目标期刊或会议的以往论文为判断参照。

为了使得paper reproducable，一般都会提供源代码。把代码打包到github，然后在introduction的最后说明 The source code is available on \url{https://github.com/.../....}
这样审稿人也会觉得你很有诚意，目前受限于中美贸易争端，可以考虑bitbucket.org或者gitee.com等其他代码管理网站。
如果对自己的paper比较有信息，投稿之后可以把自己的稿件同样上传到arxiv上面，至少昭告天下已经占坑了。
正文可能因为篇幅限制而不能展示的效果可以另外贴在Arxiv版本的后面，这样别人找到你的paper看就会有惊喜。

提交论文是这样子的。有些期刊要求是提供latex源文件在线编译，有些是你自己提供编译好的pdf，通过了之后再提供源文件和图片。一次性全部交上也没问题。Elsevier的期刊要求另外添加不超过一定单词数量的highlight，用doc文件提供，可以把contribution复制过来修改一下。提交的时候还需要一个cover letter。
论文提交的系统一般都很慢，有一些morality的东西需要勾选，也可能会出意外。因此deadline前几天提交是最明智的选择。

提交的时候可以把引用的文章，尤其是正文多次提及的文章，或者 further details can be found in [5]  的[5]打包在supplimental material中


# Rebuttal & Response 攻略

## 总体流程

递交paper之后在第一轮审稿如果没有被拒，那么就需要对review意见进行rebuttal或者response，重新提交给第二次审稿。

总体来说分成以下几个阶段
* 总结阶段（整理审稿人意见）
* 初稿阶段（第一版回复意见，基本内容确定）
* 修饰阶段（第二版回复意见，表达清楚，无语法错误）
* 提交阶段（交换审阅，导师审阅，按意见修改，然后提交）

## 总结阶段

一般审稿意见会先给出一个相对短的总体评价，来表示其对来稿的总体态度。这也可能没有。之后reviewer会对文章进行一次总结，表示自己对本文的总体理解。接着是相对具体的总体评论，比如什么well written等等，一般是较好的评价。接下来才是comment，就是质疑或者有疑问的地方，还有应该进一步说明或者需要修改的地方。最后可能会有minor correction，比如小的语法错误等等。

在本阶段，就是要鉴别出我们需要针对回复的内容有哪些。所以，第一步，看看reviewer在各个表示自己对本文理解的地方，检查是否有跟你理解不同的，并且看看这样不同是因为我们表述问题，还是reviewer就是没看懂或者理解错了。第二步，如果reviewer可能没有把comment逐点列出，这是需要我们整理。并且，我们需要理解reviewer有疑问的、质疑的、乃至反对的到底是什么点。这些就是我们之后回复的依据。第三步，按照minor correction的内容，标记文章需要修改的地方，并且全文检查是否还有其他地方犯了类似错误。总体说，本阶段要总结comment的内容，还有reviewer对本文理解并不准确的地方。

## 初稿阶段

本阶段按照上阶段总结的内容按点编写rebuttal，逐点对照回复。由于conference会有rebuttal的字数限制，写的时候要言简意赅，而且其他审稿人相互看到review和回复意见。Journal paper的response没有限制，而且还可以修改paper之后再一并提交，因此相对轻松，但是要求更严谨。

- 在按点回答的时候，要注意控制自己的情绪。不能用第二人称you来称呼reviewer，也不可以在里面评论reviewer本人。
- 使用we开头的句子来强调你做的观点或者你的贡献，但是不宜连串使用。
- 如果要指代reviewer的观点，可以用The reviewer states that 等等来指代。
- 大部分时候，最好用客观的实体作为主语，显得表述的事实比较客观。
- Rebuttal和response回答应该仅仅基于paper里面已经写的内容，或者主题相关的内容，其他与paper无关的内容不必牵涉。
- Conference rebuttal的空间很有限，适当引用已有成果的结论而不是自己复述可以有效节省空间。
- Rebuttal同样在双盲审稿的框架下进行，注意不要暴露自己。
- 对于被reviewer戳到了痛处的地方，认错认怂是首选，因为至少表示你愿意客观对待paper里面不足的地方。可能因为有这样的错误而得不到发表，这是难以避免的。但即使你硬杠死赖着说不定也不会得到好的结果。当然，可能因为这点小错而其他地方还OK，reviewer觉得你态度可以就认为你的工作是初步探索，而允许在著名不严谨的地方的情况下accept。
- 面对你的paper有好感的reviewer可以相对认怂更多，因为对方肯定你的paper，小地方认错无伤大雅。对你的paper本来不太认同的reviewer，态度可以相对强硬，这样对方反而被你镇服。当然，关键还在于回复的内容。

## 修饰阶段

修饰阶段按照正文的修饰阶段要求来修改。

## 提交阶段

到最后的时间点可能提交的人很多而刷不出来，因此应该提前相互审阅，赶紧定稿而提交。

## 客套例句
- We appreciate the generosity of the anonymous reviewers. They provided many insightful comments and helpful suggestions. We respond to each reviewer separately.




# rebuttal的攻略第二版

- 心态：审稿人不是敌人，而是帮助你完善paper的，因此心态首先放平和。
- 形式：5000/8000 characters的纯文本，或者一页pdf（CVPR）。一般逐个审稿人按点回复，当问题比较统一或者问题比较多，则按点总结统一回答。但是要保证不要漏点。
- 回答的总体思路：一句话概括回答的主要内容，之后再详细论述。
- 策略：
尝试根据review内容猜测审稿人问题背后的意图是什么，到底关注的是什么，ta想得到什么回答。很多时候，审稿人只是无聊找话说，因此区分哪些是真正的关键点、审稿人真正关注的地方，从而确定篇幅的安排、额外实验时间安排和重点回复部分。AC会让审稿人关注真正重要的点而非要求面面俱到。

## 具体情形应对策略

* 对于审稿人理解不清或者理解有误，则正面回答，并且指出文章那个地方详细说明了，或者revised version会补充。
* 对于质疑contribution仅incremental，或者性能增加不多，可以说“我们新增加的东西有intuition和insight的，这些insight如何重要，效果提升在什么方面是significant的，有什么新的特性”。reviewer可能认为，目前做得还不够好不够先进，你的是horse但ta要unicorn，可以回复进一步工作的可能性和challenge在哪里，会有什么困难，而目前工作已经做到什么地步，强调已经是SOTA，而我们的后续工作会怎么探索。
* novelty 或者 contribution 不足，有可能是审稿人没有看懂或者不对胃口，因此可能要揣测对方背景。尝试联系对方背景来重新梳理和突出文章的贡献，强调motivation和intuition，澄清方法是well-motivated 而非trivial的combine。如果只是针对某个component的novelty，那么强调其他components或者方法整体是新的，整个方法的思路是simple yet effective的。如果其他审稿人认为有优点，应该引用来辅助说明，使得审稿人重新考量。
* 确实有错误的，factual error。一般写作时尽量避免这样的情况出现，真的遇到可以大方承认并感谢，承诺会更正，如何更正，说明这个和文章原有逻辑如何衔接，是否影响文章结论，其他逻辑链条如何修改。如果是审稿人看错了，或者确实没有写明白，就承认自己写的不够好，说明之后改成什么样的描述，说一下并不是审稿人想的那样，而是什么样，有什么区别。
* performance 提升不足。一则无证据的裸喷涨点不足；二则有证据（提供了reference）喷涨点不足或喷没有对比reference中结果。针对一，可找些证据（如列reference）论证自己方法的涨点幅度和其他state-of-the-art的涨点幅度是可比的，“你看，别人发在顶会的结果相比baseline也是涨这么多”；针对二，可试着找出这些“证据”方法和自己方法的不同之处或实验细节的不公平之处，比如图像分辨率不同、backbone不同等。还有一种回复方法是承认自己的performance确实上升不够，但是至少comparable，因此提供了新的思路，而且该问题如何challenging如何重要，因此需要新的alternative。并且现有指标存在缺陷，看细节或者qualitative比较还是有不一样。
* 认为实验不足。这个也是需要尽量避免的，尝试在SM里面补充更为完备的实验。如果是在被说了，有条件做实验的，rebuttal中补上即可，虽然被认为rebuttal不应该做额外实验但审稿人都会无视。需要注明实验的setting是什么，结果是什么，结果意味着什么，印证了自己的还是审稿人的观点。若实验规模太大，rebuttal期间无条件做出，可在rebuttal中承诺final version中补上（这样力度会相对较弱）。而对于要求不合理的实验意见，可实事求是的说明为何无需做实验，根据逻辑上的想法或者直接推导即可。
* 首次给分是borderline，并且说“如果解决了xxx，我就会提升评分”，对此一定要充分争取。审稿人意见相悖，虽然可以点出作为补充，但审稿人不太可能会为此debate，因此关键还是对negative的审稿人放篇幅说明。


如果审稿人的意见中有错误、自相矛盾、带有与论文无关的歧视、或者无理据reject，可以使用AC message。只是AC需要考虑的太多，因此作用也比较有限。
- Please note that Assigned Reviewer #id has made some statements that are either against the common-sense in our field or self-contradictory (ironically his/her own confidence rating is "very confident"). blabla
- We want to bring to your attention the very flawed review #id. This reviewer is self-contradictory, cf. Comment #id1, Comment #id2, and Response #id. blabla
- We would like to raise attention to AC that unfortunately Reviewer #id holds a very biased view towards the contributions of our paper. blabla

## Rebuttal撰写流程

### 阅读整理阶段

收到review，先大致扫一遍总体评价，再逐个阅读，评估一下是否有rebuttal的需要。之后整理review中的观点，正面的和反面的都要总结，分为Strength、Weakness、Details和Justification等几方面分别总结列出，注意不要遗漏，把共同的点都连起来标注。此外，客观评分也需要记录列出来，作为后续参考。评估每个reviewer的水平，他们的confidence，他们评分的上升和下降可能。重点关注可能不confident而降分的和可能加分的。

### 正反对照和用意揣测

对照正反两方面意见，看看每个reviewer本身是否有矛盾，reviewers之间的评论是否造成冲突，或者是否有共同的问题。进一步揣测每个reviewer的问题用意，如何提高客观评分，他们可能希望什么回答，什么样的回答才可以让他们信服或者修改评分，拟定基本的回答思路。

### 实验补充和草稿撰写

根据review准备补充实验，评估实验时间和效果，按照重要性开始跑实验，确定最终实验结果展示形式（表格或者图片）。开始撰写草稿，确定是逐个审稿人逐点回答还是合并回答。先不用管字数，尽量把需要写的内容先列出来，尽量保证思路完整，之后再逐步精简，有侧重地保留或者联系上下文内容。审稿人多的时候，需要精简问题来回答。

草稿的每一段需要总结review，要用一句话或者一个短语归纳出来，之后再总结自己的回复，再写具体内容，实在篇幅有限也可以不总起直接写。先保证逻辑，之后保证没有语法错误。

rebuttal的语言要精炼，逻辑链条要短，结构清晰小巧，涉及名词要准确到位，可以使用缩写(esp for especailly)，避免长难句。使用pdf的话可以使用图片。

### 终稿阶段

上来先感谢审稿人，然后总结复述审稿人说过的strength，然后才是上面的草稿。注意字句不带情绪，就事论事，正面回答，突出自己的贡献，联系正文和SM的内容，承诺会做的都要直接给出修改内容，注意最终字数和页数。整理阶段的内容对比是否有遗漏，每一个小问题都最好覆盖到。

## 例句

### 开头

- Thank you for your suggestion.
- Thank you for the positive/detailed/constructive comments.
- We sincerely thank all reviewers and ACs for their time and efforts. Below please find the responses to some specific comments.
- We thank the reviewers for their useful comments. The common questions are first answered, then we clarify questions from every individual review.
- We thank the useful suggestions from the reviewers. Some important or common questions are first addressed, followed by answers to individual reviews.

### 表达同意

- We thank the reviewer for pointing out this issue.
- We agree with you and have incorporated this suggestion throughout our paper.
- We have reflected this comment by …
- We can/will add/compare/revise/correct ... in our revised manuscript/our final version.
- Due to the rebuttal policy, “authors should not include new experimental results in the rebuttal”, additional results may not be included. However, we will add these mentioned experiments and discussions in our final version. Thank you for the constructive comment.（对于CVPR/ICCV/ECCV rebuttal不能提供新结果的政策）

### 表达不同意

- We respectfully disagree with Reviewer #id that ...
- The reviewer might have overlooked Table #id ...
- We can compare ... but it is not quite related to our work ...
- We have to emphasize that ...
- The reviewer raises an interesting concern. However, our work ...
- Thank you for the comment, but we cannot fully agree with the comment. As stated/emphasized ...
- You have raised an important point; however, we believe that ... would be outside the scope of our paper because …
- This is a valid assessment of …; however, we believe that ... would be more appropriate because ... 

### 解释澄清

- We have indeed stated/included/discussed/compared/reported/clarified/elaborated ... in our original paper ... (cf. Line #id).
- As we stated in Line #id, ...
- We have rewritten ... to be more in line with your comments. We hope that the edited section clarifies …

### 额外信息与解释

- We have included a new figure/table (cf. Figure/Table #id) to further illustrate…
- We have supplemented the xxx section with explanations of ...
- Thank you for the comment. We will explore this in future work.

## 总结
rebuttal是一个被动防御和补救，最好是在文章写作的时候可以预先考虑审稿人会问到的地方，事先预防，或者在SM中准备好内容在rebuttal的时候引用，使用rebuttal来力挽狂澜的例子并不多，rebuttal能够决定生死的情况也很罕见，顶会可能第一稿一锤定音，rebuttal只是审稿人用以确定自己选择的过程。因此写文章的时候就要考虑rebuttal的事宜，整体考量。


参考 https://zhuanlan.zhihu.com/p/104298923


[^1]: Xiaofeng Ling, Qiang Yang "学术研究--你的成功之道"

[^2]: Mike Ashby, How to write a paper

[^3]: http://tcse.cn/~wsdou/advice/advice/deadline-chen.pdf

[^4]: https://www.cs.cmu.edu/~jrs/sins.html

# 已知但没来得及整理的内容

- 文章算是开拓一个新方向的话，可以第二章就说方法，后面再来related work，在intro部分提及一个the work closest to ours is 
- 摘要可以是发问式的把文章关注的问题说出来，不过是具体解决的问题还是可能上位大问题
- Figure 引用和本体最好在同一页
- 点出的问题、解决方法和例子要联系一起
- 几乎段首句都是总结形句子
- 尽量不强调自己用了别人的东西
- 期刊写作建议：学会把论文写长、但又看起来也不冗余（图表、公式），尽量把参考文献写全，实验做全


# checklist
- 是否有两个空格
- 所有图表都被引用了吗？没有正文，图表的意思传达是否受影响？有没有标号？
- 所有符号和缩写都定义过了吗？是否在相隔很远的地方重复了一下定义？
- 所有大标题小标题是否首字母大写了？是否有明显错误？
- 所有的公式都没有出格，而且已经符号都定义清楚了？是否有标号？
- 是否超页数了？图和表格的位置是否对？和之前版本对照一下。
- 引用是否规范
- 论文的id和系统里面得到的是否一样？


<!-- 
Your paper will get rejected unless you make it very clear, up front, what you think your paper has contributed. If you don't explicitly state the problem you're solving, the context of your problem and solution, and how your paper differs (and improves upon) previous work, you're trusting that the reviewers will figure it out. You must make your paper easy to read. You've got to make it easy for anyone to tell what your paper is about, what problem it solves, why the problem is interesting, what is really new in your paper (and what isn't), why it's so neat. 




(1) Start by stating which problem you are addressing, keeping the audience in mind.  They must care about it, which means that sometimes you must tell them why they should care about the problem.  
(2) Then state briefly what the other solutions are to the problem, and why they aren't satisfactory.  If they were satisfactory, you wouldn't need to do the work.  
(3) Then explain your own solution, compare it with other solutions, and say why it's better.  
(4) At the end, talk about related work where similar techniques and experiments have been used, but applied to a different problem.  


Again, stating the problem and its context is important. But what you want to do here is to state the "implications" of your solution. Sure it's obvious....to you. But you run the risk of misunderstanding and rejection if you don't spell it out explicitly in your introduction.
How can you protect yourself against these mistakes? You must make your paper easy to read. You've got to make it easy for anyone to tell what your paper is about, what problem it solves, why the problem is interesting, what is really new in your paper (and what isn't), why it's so neat. And you must do it up front. In other words, you must write a dynamite introduction. In your introduction you can address most of the points we talked about in the last section. If you do it clearly and succinctly, you set the proper context for understanding the rest of your paper. Only then should you go about describing what you've done. 


Underutilized technique:  explain the main idea with a simple, toy example
Show simple toy examples to let people get the main idea
在正式method之前


If you have a planned organization for your discussion and you not only stick to it, but tell your readers over and over where you are in that organization, you'll have a well written paper
motivate the reader for what follows
perhaps the most important principle of good writing is to keep the reader uppermost in mind: what does the reader knows so far? what does the reader expect next and why?



It should be easy to read the paper in a big hurry and still learn the main points. The figures and captions can help tell the story.  So the figure captions should be self-contained and the caption should tell the reader what to notice about the figure.


CVPR常见的拒绝理由：
Quick and easy reasons to reject a paper
•Do the authors promise more than they deliver?
•Are there some important references that they don’t mention (and therefore they’re not up on the state-of-the-art for this problem)?
•Has their main idea been done before by someone else?
•Are the results incremental (too similar to previous work)?
•Are the results believable (too different than previous work)?
•Is the paper poorly written?  
•Do they make incorrect statements?

Be honest, scrupulously honestConvey the right impression of performance.  
这说明他们知道的了解到的能够识破的弄虚作假太多了


Good writing is re-writing.  This means you need to start writing the paper early!

平均的都汇报标准差


术语表方便统一表达


This paper clearly states what claims are being investigated.
This paper explains how the results substantiate the claims.
This paper explicitly identifies limitations or technical assumptions.
This paper includes a conceptual outline and/or pseudocode description of AI methods introduced.
Does this paper make theoretical contributions?
	All assumptions and restrictions are stated clearly and formally.	
	All novel claims are stated formally (e.g., in theorem statements).
	Proofs of all novel claims are included.
	Proof sketches or intuitions are given for complex and/or novel results.
	Appropriate citations to theoretical tools used are given.

Does this paper rely on one or more data sets?
	All novel datasets introduced in this paper are included in a data appendix.
	All novel datasets introduced in this paper will be made publicly available upon publication of the paper with a license that allows free usage for research purposes.
	All datasets drawn from the existing literature (potentially including authors’ own previously published work) are accompanied by appropriate citations.
	All datasets drawn from the existing literature (potentially including authors’ own previously published work) are publicly available.
	All datasets that are not publicly available are described in detail.



Does this paper include computational experiments?

	All source code required for conducting experiments is included in a code appendix.
	All source code required for conducting experiments will be made publicly available upon publication of the paper with a license that allows free usage for research purposes.
	If an algorithm depends on randomness, then the method used for setting seeds is described in a way sufficient to allow replication of results.
	This paper specifies the computing infrastructure used for running experiments (hardware and software), including GPU/CPU models; amount of memory; operating system; names and versions of relevant software libraries and frameworks.
	This paper formally describes evaluation metrics used and explains the motivation for choosing these metrics.
	This paper states the number of algorithm runs used to compute each reported result.
	Analysis of experiments goes beyond single-dimensional summaries of performance (e.g., average; median) to include measures of variation, confidence, or other distributional information.
	This paper lists all final (hyper-)parameters used for each model/algorithm in the paper’s experiments.
	This paper states the number and range of values tried per (hyper-)parameter during development of the paper, along with the criterion used for selecting the final parameter setting. 


+- 标准差可以缩小
ae结构可以说成是hourglass网络（沙漏）


补充如何写支撑材料


所有专有名词和缩写标出来，列一个表格，确保了每个地方都有解释

首先指出reviewer讲的问题是在文章哪个部分，再来说回应 -->